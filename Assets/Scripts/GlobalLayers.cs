﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalLayers
{
    public static int _ObstacleLayer = LayerMask.NameToLayer("Obstacle");
    public static int _PlayerLayer = LayerMask.NameToLayer("Player");
    public static int _CollectibleLayer = LayerMask.NameToLayer("Collectible");
}
