﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Player _Player = null;
    [SerializeField]
    private float _MinPosPlayer = 2.5f;

    [SerializeField]
    private Transform _CameraTargetFPV = null;

    private int     _LineX = 0;
    private float   _RotX = 25f;
    private Vector2 _TargetPos = new Vector2();
    private Vector2 _CameraPos = new Vector2();
    private Vector2 _Forward = new Vector2(1,0);
    private float _MinPosY = 0f;

    private Vector3     _StartPositionRun = Vector3.zero;
    private Quaternion  _StartRotationRun = Quaternion.identity;

    private CameraState _CurrentState = CameraState.None;

    private void Start()
    {
        _Player.OnLineChanged += OnLineChanged;
        _MinPosY = transform.position.y;

        _StartPositionRun = transform.position;
        _StartRotationRun = transform.rotation;

        _CurrentState = CameraState.TPV;
        GameController.Get().OnSectionTypeChanged += OnSectionTypeChanged;
    }

    public void LateUpdateCC()
    {
        if (_CurrentState == CameraState.TPV)
        {
            Vector3 position = transform.position;
            position.x = Mathf.Lerp(position.x, _LineX, 0.3f);
            float tempY = _Player.transform.position.y - _MinPosPlayer;
            tempY = Mathf.Clamp(tempY, 0f, 10f);
            position.y = _MinPosY + tempY;

            transform.position = position;
        }
    }

    private void OnLineChanged(int pLine)
    {
        _LineX = pLine - 1;
    }

    private void SetCameraState(CameraState pState)
    {
        _CurrentState = pState;
    }

    private void OnSectionTypeChanged(SectionType pSection)
    {
        switch (pSection)
        {
            case SectionType.Shoot:
                StartCoroutine(BlendCamera(_CameraTargetFPV.position, _CameraTargetFPV.rotation, CameraState.FPV));
                break;
            //case SectionType.Run:
            case SectionType.PreRun:
                StartCoroutine(BlendCamera(_StartPositionRun, _StartRotationRun, CameraState.TPV));
                break;
        }
    }

    private IEnumerator BlendCamera(Vector3 pTargetPosition, Quaternion pTargetRotation, CameraState pTargetState)
    {
        SetCameraState(CameraState.Blending);
        float time = 0.5f;
        float currentTime = 0f;
        Vector3 startPos = transform.position;
        Quaternion startRot = transform.rotation;

        while (true)
        {
            currentTime += Time.deltaTime;
            float lerpTime = currentTime / time;
            transform.position = Vector3.Lerp(startPos, pTargetPosition, lerpTime);
            transform.rotation = Quaternion.Lerp(startRot, pTargetRotation, lerpTime);

            if (currentTime > time)
            {
                break;
            }

            yield return null;
        }

        SetCameraState(pTargetState);
    }
}
