﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerFirewallController : MonoBehaviour
{
    [SerializeField] private Camera                 _Camera = null;
    [SerializeField] private PlayerPickUpController _PlayerPickUpController = null;

    private bool    _Shoot = false;
    private Vector3 _ShootPos = new Vector3();

    public void Shoot()
    {
        _Shoot = false;

#if UNITY_EDITOR

        _Shoot = Input.GetMouseButtonDown(0);
        _ShootPos = Input.mousePosition;

#elif UNITY_ANDROID

        if (Input.touches.Length > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                _Shoot = true;
                _ShootPos = touch.position;
            }
        }

#endif

        if (_Shoot && 
            _PlayerPickUpController.GetCollectiblesCount(CollectibleType.Bullet) > 0)
        {
            RaycastHit hit;
            Ray ray = _Camera.ScreenPointToRay(_ShootPos);
            if (Physics.Raycast(ray, out hit, 20f))
            {
                Virus virus = hit.collider.GetComponent<Virus>();
                if (virus != null)
                {
                    virus.Destroy(true);
                    Debug.LogError("virus hit");
                }
            }

            _PlayerPickUpController.RemoveCollectibles(CollectibleType.Bullet, 1);
        }
    }
}
