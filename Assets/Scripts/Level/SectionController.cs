﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

/// <summary>
/// Schopnosti:
/// - zpomaleni kulicek na firewallu (firewall kulicku checkne stejne rychle ale bude ji dyl drzet)
/// - magnet na coiny a naboje
/// - double jump
/// 
/// Gameplay feature:
/// - prestige hry 100 levelu na prestige, druha prestige prida novou gameplay element, obstacli na corridorech se budou hybat
/// - multipliery na skore 2x, 3x, 4x, apod
/// - vyssi obstacly maji vic coinu
/// - specialni virus kterej je vetsi a pokud ho hrac trefi tak odemkne kolekci jednoho typu viru, kterou kdyz hrac nasbira strilenim toho viru tak ziska odmenu
/// - jakmile dojdou naboje tak se vypne firewall a pohyb viru se zrychli
/// - jakmile hrac trefi tri data, firewall se vypne a hraci nepomaha pri zbrzdovani viru
/// 
/// </summary>
public class SectionController : MonoBehaviour
{
    public Action<SectionType> OnSectionChanged;

    private SectionType _CurrentSectionType = SectionType.None;

    private float _CurrentRunTime = 0f;
    private float _RandomRunTime = 0f;

    private int _RunSectionCount = 0;
    private int _ShootSectionCount = 0;

    public SectionType SectionType => _CurrentSectionType;

    private void Awake()
    {
        _RunSectionCount++;
        _RandomRunTime = Random.Range(30f, 50f);

        GameController.Get().OnLastCorridorReached += OnLastCorridorReached;
        GameController.Get().OnVirusSectionDataChanged += OnVirusSectionDataChanged;
    }

    public void UpdateSC()
    {
        if (_CurrentSectionType == SectionType.Run)
        {
            _CurrentRunTime += GameController.Get().DeltaTime;
            if (_CurrentRunTime > _RandomRunTime)
            {
                SetSection(SectionType.PreShooter);
            }
        }
    }

    public void SetSection(SectionType pSection)
    {
        if (pSection == _CurrentSectionType)
            return;

        _CurrentSectionType = pSection;

        switch (pSection)
        {
            case SectionType.Run:
                _RunSectionCount++;
                _CurrentRunTime = 0f;
                _RandomRunTime = Random.Range(30f, 50f);
                break;
            case SectionType.Shoot:
                _ShootSectionCount++;
                break;
        }

        if (OnSectionChanged != null)
        {
            OnSectionChanged(pSection);
        }
    }

    private void OnLastCorridorReached()
    {
        SetSection(SectionType.Shoot);
    }

    private void OnVirusSectionDataChanged(VirusSectionData pData)
    {
        if (pData._MaxVirusCount == pData._CurrentAllDestroyed)
        {
            float percent = (float)pData._CurrentVirusDestroyedByPlayer / (float)pData._MaxVirusCount;
            if (percent > 0.05f)
            {
                SetSection(SectionType.PreRun);
            }
            else
            {
                //reklama !!!!!!!!!!!
                SceneManager.LoadScene("MainLevel");
            }
        }
    }
}
