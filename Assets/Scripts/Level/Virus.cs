﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class Virus : MonoBehaviour
{
    public Action<bool, VirusType> OnVirusDestroyed; // bool == true -> Player hit

    private bool _IsActive = false;
    public bool IsActive
    {
        get { return _IsActive; }
    }

    private float               _MoveOffset = 25f;
    private float               _CurrentFirewallTime = 0f;
    private FirewallCheckType   _CheckedByFirewall = FirewallCheckType.None;
    private VirusType           _CurrentVirusType = VirusType.None;

    public void Init(Vector3 pPosition, bool pActive, VirusType pVirus)
    {
        _CheckedByFirewall = FirewallCheckType.None;
        transform.position = pPosition;
        _MoveOffset = Random.Range(10f, 50f);
        SetActive(pActive);
        _CurrentVirusType = pVirus;
    }

    public void UpdateVirus()
    {
        if (transform.position.z < 20f && _CheckedByFirewall == FirewallCheckType.None)
        {
            _CheckedByFirewall = FirewallCheckType.Testing;
        }

        switch (_CheckedByFirewall)
        {
            case FirewallCheckType.None:
            case FirewallCheckType.Checked:
                float moveOffset = _MoveOffset * GameController.Get().DeltaTime;
                Vector3 currentPosition = transform.position;
                currentPosition.z -= moveOffset;
                transform.position = currentPosition;
                break;
            case FirewallCheckType.Testing:
                _CurrentFirewallTime += GameController.Get().DeltaTime;
                if (_CurrentFirewallTime > 2f)
                {
                    _CheckedByFirewall = FirewallCheckType.Checked;
                    _CurrentFirewallTime = 0f;
                }
                break;
        }

        if (transform.position.z < -5f)
        {
            Destroy(false);
        }
    }

    public void Destroy(bool pHitByPlayer)
    {
        SetActive(false);
        OnVirusDestroyed?.Invoke(pHitByPlayer, _CurrentVirusType);
    }

    private void SetActive(bool pActive)
    {
        _IsActive = pActive;

        gameObject.SetActive(pActive);
    }
}
