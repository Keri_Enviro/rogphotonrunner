﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerPickUpController : MonoBehaviour
{
    public Action<Dictionary<CollectibleType, int>> OnCollectiblesChanged;

    private Dictionary<CollectibleType, int> _Collectibles = new Dictionary<CollectibleType, int>();

    public void AddCollectibles(CollectibleType pCollectible, int pCount)
    {
        if (_Collectibles.ContainsKey(pCollectible))
        {
            _Collectibles[pCollectible] += pCount;
        }
        else
        {
            _Collectibles.Add(pCollectible, pCount);
        }

        OnCollectiblesChanged?.Invoke(_Collectibles);
    }

    public void RemoveCollectibles(CollectibleType pCollectible, int pCount)
    {
        if (_Collectibles.ContainsKey(pCollectible))
        {
            _Collectibles[pCollectible] -= pCount;

            OnCollectiblesChanged?.Invoke(_Collectibles);
        }
    }

    public int GetCollectiblesCount(CollectibleType pCollectible)
    {
        int count = 0;
        _Collectibles.TryGetValue(pCollectible, out count);

        return count;
    }
}
