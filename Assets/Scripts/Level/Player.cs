﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    public Action<Obstacle>                         OnPlayerHit;
    public Action<int>                              OnLineChanged;
    public Action<Dictionary<CollectibleType, int>> OnCollectiblesChanged;

    [SerializeField] private CharacterController        _CharacterController = null;
    [SerializeField] private float                      _JumpHeight = 4f;
    [SerializeField] private float                      _Gravity = 9.81f;
    [SerializeField] private float                      _Speed = 10f;
    [SerializeField] private PlayerHitController        _PlayerHitController = null;
    [SerializeField] private PlayerFirewallController   _PlayerFirewallController = null;
    [SerializeField] private PlayerPickUpController     _PlayerPickUpController = null;
    [SerializeField] private Animator                   _Animator = null;

    private int     _LineIndex = 1;
    private Vector3 _Move = Vector3.zero;
    private bool    _IsGrounded = false;
    private int     _FrameCount = 0;

    private bool _MoveLeft = false;
    private bool _MoveRight = false;
    private bool _Jump = false;
    private bool _DragDown = false;
    private bool _Slide = false;
    private bool _Sliding = false;

    private Vector2 _StartTouchPos;
    private Vector2 _SwipeDelta;
    private bool    _CanSwipe = false;

    [SerializeField]
    private float[] _PositionsX = new float[]
    {
        -2, //0
        0, //1
        2, //2
    };

    private void Start()
    {
        _PlayerHitController.Init(this);
        _PlayerPickUpController.OnCollectiblesChanged += CollectiblesChanged;
        GameController.Get().OnSectionTypeChanged += OnSectionTypeChanged;
    }

    public void UpdatePlayer()
    {
        if (GameController.Get().SectionType == SectionType.Shoot ||
            GameController.Get().SectionType == SectionType.PreRun)
        {
            _PlayerFirewallController.Shoot();
        }
        else
        {
            UpdateInput();
        }

        MovePlayer();
    }

    public void PlayerHit(Obstacle pObstacle)
    {
        if (OnPlayerHit != null)
        {
            OnPlayerHit(pObstacle);
        }
    }

    public void AddCollectibles(CollectibleType pCollectible, int pCount)
    {
        _PlayerPickUpController.AddCollectibles(pCollectible, pCount);
    }

    public void RemoveCollectibles(CollectibleType pCollectible, int pCount)
    {
        _PlayerPickUpController.RemoveCollectibles(pCollectible, pCount);
    }

    private void UpdateInput()
    {
        _MoveLeft = _MoveRight = _Jump = _DragDown = _Slide = false;

#if UNITY_EDITOR
        _MoveLeft = Input.GetKeyDown(KeyCode.A);
        _MoveRight = Input.GetKeyDown(KeyCode.D);
        _Jump = Input.GetKeyDown(KeyCode.Space) && _IsGrounded;
        _DragDown = Input.GetKeyDown(KeyCode.S) && !_IsGrounded;
        _Slide = Input.GetKeyDown(KeyCode.S) && _IsGrounded;

#elif UNITY_ANDROID
        
        if (Input.touches.Length > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                _StartTouchPos = _SwipeDelta = touch.position;
                _CanSwipe = true;
            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                _StartTouchPos = new Vector2();
                _SwipeDelta = new Vector2();
                _CanSwipe = false;
            }

            _SwipeDelta = touch.position - _StartTouchPos;
            if (_SwipeDelta.magnitude > 100 && _CanSwipe)
            {
                if (Mathf.Abs(_SwipeDelta.x) > Mathf.Abs(_SwipeDelta.y))
                {
                    if (_SwipeDelta.x > 0)
                    {
                        _MoveRight = true;
                    }
                    else
                    {
                        _MoveLeft = true;
                    }
                }
                else
                {
                    if (_SwipeDelta.y > 0)
                    {
                        _Jump = _IsGrounded;
                    }
                    else
                    {
                        _DragDown = !_IsGrounded;
                        _Slide = _IsGrounded;
                    }
                }

                _CanSwipe = false;
            }
        }

#endif
    }

    private void MovePlayer()
    {
        _FrameCount++;
        _IsGrounded = _CharacterController.isGrounded;
        Vector3 currentPosition = transform.position;
        Vector3 targetPosition = currentPosition;
        targetPosition.x = 0;

        if (_MoveLeft)
        {
            _LineIndex--;
        }
        else if (_MoveRight)
        {
            _LineIndex++;
        }

        _LineIndex = Mathf.Clamp(_LineIndex, 0, 2);
        if (OnLineChanged != null)
        {
            OnLineChanged(_LineIndex);
        }

        targetPosition.x = _PositionsX[_LineIndex];
        _Move.x = (targetPosition - currentPosition).x * _Speed;

        if (!_CharacterController.isGrounded && _FrameCount % 2 == 0)
        {
            RaycastHit hit;
            if (Physics.SphereCast(currentPosition, 0.5f, Vector3.down, out hit, 1.2f))
            {
                _IsGrounded = true;
            }
        }

        if (_Jump)
        {
            _Move.y = _JumpHeight;
        }

        if (_DragDown)
        {
            _Move.y = -_JumpHeight;
        }
        else if(_Slide && !_Sliding)
        {
            _Animator.SetTrigger("Slide");
            StartCoroutine(SlideDown());
        }

        if (!_CharacterController.isGrounded)
        {
            _Move.y -= _Gravity * GameController.Get().DeltaTime;
        }

        _CharacterController.Move(_Move * GameController.Get().DeltaTime);

        _PlayerHitController.UpdatePHC();
    }

    private void CollectiblesChanged(Dictionary<CollectibleType, int> pCollectibles)
    {
        OnCollectiblesChanged?.Invoke(pCollectibles);
    }

    private void OnSectionTypeChanged(SectionType pSection)
    {
        switch (pSection)
        {
            case SectionType.Run:
                _Animator.SetTrigger("Run");
                break;
            case SectionType.Shoot:
                _Animator.SetTrigger("ShootStand");
                break;
            case SectionType.PreRun:
                _Animator.SetTrigger("StandIdle");
                break;
        }
    }

    private IEnumerator SlideDown()
    {
        _Sliding = true;
        _CharacterController.height = 1f;
        Vector3 pos = _Animator.transform.localPosition;
        pos.y = -0.5f;
        _Animator.transform.localPosition = pos;

        float time = 1f;
        float currentTime = 0f;
        while (true)
        {
            currentTime += GameController.Get().DeltaTime;

            if (currentTime > time)
            {
                break;
            }

            yield return null;
        }

        _Sliding = false;
        _CharacterController.height = 2f;
        pos = _Animator.transform.localPosition;
        pos.y = -1f;
        _Animator.transform.localPosition = pos;
    }
}