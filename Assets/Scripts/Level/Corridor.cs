﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Corridor : MonoBehaviour
{
    //ma promennou ve ktere jsou ulozeny 3 pozice kde se muze spawnovat prekazka
    //ma mit metodu ktera nahodne (Random) vybere pozici a vytvori novej gameobject treba kostku a vlozi ji na pozici
    //ale musi ji vlozit tak aby ta kostka byla zaroven potomek toho corridoru
    // Instantiate(kostka, pozice, rotaci, transform); 
    // GameObject go = Instantiate(kostka, pozice, rotace); go.transform.SetParent(transform);

    [SerializeField] private Transform[]    _ObstaclePlaces = null;
    [SerializeField] private Transform[]    _CollectiblePlaces = null;
    [SerializeField] private Transform      _Connection = null;
    [SerializeField] private CorridorType   _CorridorType = CorridorType.None;
    [SerializeField] private CorridorType[] _AllowedCorridorTypes = null;

    private GameObject          _Obstacle = null;
    private List<GameObject>    _Collectibles = new List<GameObject>();
    private bool                _IsActive = true;
    private int                 _UniqueID = -1;

    public bool             IsActive => _IsActive;
    public CorridorType     CorridorType => _CorridorType;
    public CorridorType[]   AllowedCorridorTypes => _AllowedCorridorTypes;
    public int              UniqueID => _UniqueID;
    public Transform        Connection => _Connection;

    public void Init(bool pActive, int pID)
    {
        SetActive(pActive);

        SetID(pID);
        switch (_CorridorType)
        {
            case CorridorType.LastCorridor:
                break;
            case CorridorType.Ground:
                break;
            case CorridorType.RandomObstacle:
                if (_Obstacle != null)
                {
                    Destroy(_Obstacle);
                }

                CreateObstacle();
                break;
            case CorridorType.WithHole:
                break;
            case CorridorType.WithObstacle:
                break;
            case CorridorType.None:
                break;
            case CorridorType.StartCorridor:
                break;
            default:
                Debug.LogError("Chybi ti nastavit corridor type!");
                break;
        }

        CreateCollectible();
    }

    public void SetID(int pID)
    {
        _UniqueID = pID;
    }

    private void CreateObstacle()
    {
        if (_ObstaclePlaces == null || !IsActive)
            return;

        int index = Random.Range(0, _ObstaclePlaces.Length);
        Vector3 obstaclePosition = _ObstaclePlaces[index].position;

        _Obstacle = GameController.Get().ObstacleGenerationController.CreateCorridorObstacle
        (
            obstaclePosition,
            Quaternion.identity,
            transform
        );
    }

    /// <summary>
    /// Temporary reseni
    /// </summary>
    private void CreateCollectible()
    {
        if (_Collectibles != null && _Collectibles.Count > 0)
        {
            for (int i = 0; i < _Collectibles.Count; i++)
            {
                Destroy(_Collectibles[i]);
            }
            _Collectibles.Clear();
        }

        if (_CollectiblePlaces == null || _CollectiblePlaces.Length == 0 || !IsActive)
            return;

        //int index = Random.Range(0, _CollectiblePlaces.Length);
        //Vector3 obstaclePosition = _CollectiblePlaces[index].position;

        for (int i = 0; i < _CollectiblePlaces.Length; i++)
        {
            GameObject go = GameController.Get().CollectibleGenerationController.CreateCollectible
            (
                _CollectiblePlaces[i].position,
                Quaternion.identity,
                transform
            );
            _Collectibles.Add(go);
        }
    }

    public void UpdateCorridor()
    {
        if (_CorridorType == CorridorType.LastCorridor)
        {
            if (transform.position.z < -3.4f)
            {
                Vector3 pos = transform.position;
                pos.z = -3.5f;
                transform.position = pos;
                GameController.Get().LastCorridorReached();
            }
            else
            {
                Move();
            }
        }
        else
        {
            Move();

            if (GameController.Get().SectionType != SectionType.Run)
            {
                if (transform.position.z < -10f)
                {
                    SetActive(false);
                }
            }
        }
    }

    public void LastMoveUp()
    {
        if(!_IsActive)
            return;

        StartCoroutine(LastMove(new Vector3(0, 1.5f, -3.5f), 1f, false));
    }

    public void LastMoveBehind()
    {
        if (!_IsActive)
            return;

        StartCoroutine(LastMove(new Vector3(0, 1.5f, -10f), 1f, true));
    }

    private IEnumerator LastMove(Vector3 pTargetPosition, float pTime, bool pDeactivate)
    {
        float currentTime = 0f;
        Vector3 currentPosition = transform.position;
        while (true)
        {
            currentTime += GameController.Get().DeltaTime;
            transform.position = Vector3.Lerp(currentPosition, pTargetPosition, currentTime / pTime);

            if (currentTime > pTime)
            {
                break;
            }

            yield return null;
        }

        if(pDeactivate)
            SetActive(false);
    }

    private void Move()
    {
        float moveOffset = 5f * GameController.Get().DeltaTime;
        Vector3 currentPosition = transform.position;
        currentPosition.z -= moveOffset;
        transform.position = currentPosition;
    }

    public void SetActive(bool pActive)
    {
        _IsActive = pActive;
        gameObject.SetActive(_IsActive);
    }
}
