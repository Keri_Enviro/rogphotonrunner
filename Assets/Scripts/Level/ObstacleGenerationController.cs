﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class ObstacleGenerationController : MonoBehaviour
{
    public Action<VirusSectionData> OnVirusSectionDataChanged;
    public Action<VirusType>        OnVirusHit;

    [SerializeField] private GameObject _ObstaclePrefab = null;
    [SerializeField] private Virus      _VirusPrefab = null;

    private List<Virus>         _Viruses = new List<Virus>();
    private VirusSectionData    _CurrentVirusSectionData = new VirusSectionData();

    private void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            CreateVirus(false);
        }

        GameController.Get().OnSectionTypeChanged += OnSectionTypeChanged;
    }

    public GameObject CreateCorridorObstacle(Vector3 pPosition, Quaternion pRotation, Transform pParent)
    {
        GameObject go = Instantiate(_ObstaclePrefab, pPosition, pRotation, pParent);

        return go;
    }

    public void UpdateCGC()
    {
        if (GameController.Get().SectionType != SectionType.Shoot)
        {
            return;
        }

        for (int i = 0; i < _Viruses.Count; i++)
        {
            Virus vir = _Viruses[i];
            if (vir.IsActive)
                vir.UpdateVirus();
        }
    }

    private void OnSectionTypeChanged(SectionType pSection)
    {
        switch (pSection)
        {
            case SectionType.Shoot:
                _CurrentVirusSectionData = new VirusSectionData();
                StartCoroutine(SpawnViruses());
                break;
        }
    }

    private Virus GetDisabledVirus()
    {
        Virus vir = null;

        for (int i = 0; i < _Viruses.Count; i++)
        {
            Virus virus = _Viruses[i];
            if (!virus.IsActive)
            {
                vir = virus;
                break;
            }
        }

        return vir;
    }

    private Virus CreateVirus(bool pActive)
    {
        Virus vir = GetDisabledVirus();

        if (vir == null)
        {
            GameObject go = Instantiate(_VirusPrefab.gameObject, new Vector3(0, 100, 0), Quaternion.identity);
            vir = go.GetComponent<Virus>();
            _Viruses.Add(vir);
            vir.Init(new Vector3(0, 100, 0), pActive, VirusType.None);
            vir.OnVirusDestroyed += OnVirusDestroyed;
        }

        return vir;
    }

    private IEnumerator SpawnViruses()
    {
        //for test
        yield return new WaitForSeconds(1f);

        //musime specifikovat pocet viru podle strel, prozatim 20
        int virusCount = 20;
        _CurrentVirusSectionData._MaxVirusCount = virusCount;

        while (virusCount > 0)
        {
            int count = Random.Range(1, 5);
            count = Mathf.Clamp(count, 0, virusCount);
            virusCount -= count;

            for (int i = 0; i < count; i++)
            {
                Virus vir = CreateVirus(true);
                Vector3 position = new Vector3(Random.Range(-5f, 5f), Random.Range(-3f, 8f), 100f);
                vir.Init(position, true, VirusType.Virus);
            }

            yield return new WaitForSeconds(Random.Range(0.5f, 2f));
        }
    }

    private void OnVirusDestroyed(bool pHitByPlayer, VirusType pVirus)
    {
        _CurrentVirusSectionData._CurrentAllDestroyed++;

        if (pHitByPlayer)
        {
            switch (pVirus)
            {
                case VirusType.Virus:
                    _CurrentVirusSectionData._CurrentVirusDestroyedByPlayer++;
                    break;
                case VirusType.Data:
                    _CurrentVirusSectionData._CurrentDataDestroyedByPlayer++;
                    break;
            }

            OnVirusHit?.Invoke(pVirus);
        }

        OnVirusSectionDataChanged?.Invoke(_CurrentVirusSectionData);
    }
}

public struct VirusSectionData
{
    public int _MaxVirusCount;
    public int _CurrentAllDestroyed;
    public int _CurrentVirusDestroyedByPlayer;
    public int _CurrentDataDestroyedByPlayer;
}
