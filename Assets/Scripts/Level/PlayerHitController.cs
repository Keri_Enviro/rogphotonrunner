﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitController : MonoBehaviour
{
    private Player  _Player = null;
    private int     _TickCount = 0;

    public void Init(Player pPlayer)
    {
        _Player = pPlayer;
    }

    public void UpdatePHC()
    {
        _TickCount++;

        if (_TickCount % 2 == 1)
            return;

        RaycastHit hit;
        if (Physics.SphereCast(transform.position, 0.5f, Vector3.forward, out hit, 0.5f))
        {
            Obstacle obstacle = hit.collider.GetComponent<Obstacle>();

            if (obstacle != null)
            {
                TryPlayerHit(obstacle, hit.point, hit.normal);
            }
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.gameObject.layer == GlobalLayers._ObstacleLayer)
        {
            Obstacle obstacle = hit.collider.GetComponent<Obstacle>();

            if (obstacle != null)
            {
                TryPlayerHit(obstacle, hit.point, hit.normal);
            }

            Deadzone deadzone = hit.collider.GetComponent<Deadzone>();
            if (deadzone != null)
            {
                TryPlayerHit(null, hit.point, hit.normal);
            }
        }
    }

    private void TryPlayerHit(Obstacle pObstacle, Vector3 pPoint, Vector3 pNormal)
    {
        float dotProduct = Vector3.Dot(Vector3.up, pNormal);

        if (dotProduct > 0.1f )
        {
            return;
        }

        _Player.PlayerHit(pObstacle);
    }
}

