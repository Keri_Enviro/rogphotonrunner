﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    [SerializeField]
    private CollectibleType _CollectibleType = CollectibleType.None;

    private int     _Count = 1;
    private bool    _IsActive = false;

    public void Init(bool pActive, int pCount = 1)
    {
        _Count = pCount;
        _IsActive = pActive;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == GlobalLayers._PlayerLayer)
        {
            Player player = other.GetComponent<Player>();

            if (player != null)
            {
                player.AddCollectibles(_CollectibleType, _Count);
                Destroy(gameObject);
            }
        }
    }
}
