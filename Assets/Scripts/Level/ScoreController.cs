﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreController : MonoBehaviour
{
    public Action<int> OnScoreDistanceChanged;
    public Action<int> OnScoreHitChanged;
    public Action<int> OnDataHitWarningChanged;

    //coiny, za coiny si hrac koupi upgrade na prodlouzeni modifier a na zvyseni modifier
    private int _ScoreDistance = 0;
    private int _ScoreHit = 0;
    private int _DataHitWarning = 4;
    private int _ScoreModifier = 1;
    private int _CurrentScoreIncrement = 5;

    private float _CurrentTime = 1f;

    private void Start()
    {
        GameController.Get().OnGameStateChanged += OnGameStateChanged;
        GameController.Get().OnSectionTypeChanged += OnSectionTypeChanged;
        GameController.Get().OnVirusHit += VirusHit;
    }

    public void Init()
    {
        _ScoreDistance = 0;
        _ScoreHit = 0;
        _DataHitWarning = 4;
        _ScoreModifier = 1;
        _CurrentScoreIncrement = 5;
    }

    public void UpdateSC()
    {
        if (GameController.Get().SectionType == SectionType.Run ||
            GameController.Get().SectionType == SectionType.PreShooter)
        {
            _CurrentTime -= GameController.Get().DeltaTime;
            if (_CurrentTime < 0f)
            {
                _ScoreDistance += _CurrentScoreIncrement;
                _CurrentTime = 1f;

                OnScoreDistanceChanged?.Invoke(_ScoreDistance);
            }
        }
    }

    public void SetScoreModifier(int pModifier)
    {
        _ScoreModifier = pModifier;
        _CurrentScoreIncrement = 5 * _ScoreModifier;
    }

    private void VirusHit(VirusType pVirus)
    {
        switch (pVirus)
        {
            case VirusType.Virus:
                _ScoreHit++;
                OnScoreHitChanged?.Invoke(_ScoreHit);
                break;
            case VirusType.Data:
                _DataHitWarning--;
                OnDataHitWarningChanged?.Invoke(_DataHitWarning);
                break;
        }
    }

    private void OnGameStateChanged(GameState pState)
    {
        switch (pState)
        {
            case GameState.Playing:

                break;
            case GameState.Paused:
            case GameState.End:
            case GameState.None:

                break;
        }
    }

    private void OnSectionTypeChanged(SectionType pType)
    {
        switch (pType)
        {
            case SectionType.Run:
                break;
            case SectionType.Shoot:
                _DataHitWarning = 4;
                break;
        }
    }
}
