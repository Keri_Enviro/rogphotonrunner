﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleGenerationController : MonoBehaviour
{
    [SerializeField]
    private Collectible[] _Collectibles = null;

    public GameObject CreateCollectible(Vector3 pPosition, Quaternion pRotation, Transform pParent)
    {
        Collectible collectible = null;

        GameObject go = Instantiate(_Collectibles[0].gameObject, pPosition, pRotation, pParent);
        collectible = go.GetComponent<Collectible>();
        collectible.Init(true, 1);

        return go;
    }
}
