﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    private static GameController _Instance = null;

    public static GameController Get()
    {
        return _Instance;
    }

    public Action<GameState>        OnGameStateChanged;
    public Action<SectionType>      OnSectionTypeChanged;
    public Action<int>              OnScoreDistanceChanged;
    public Action<int>              OnScoreHitChanged;
    public Action<int>              OnDataHitWarningChanged;
    public Action                   OnLastCorridorReached;
    public Action<VirusSectionData> OnVirusSectionDataChanged;
    public Action<VirusType>        OnVirusHit;

    public Action<Dictionary<CollectibleType, int>> OnCollectiblesChanged;

    [SerializeField] private Player                             _Player = null;
    [SerializeField] private CorridorGenerationController       _CorridorGenerationController = null;
    [SerializeField] private ObstacleGenerationController       _ObstacleGenerationController = null;
    [SerializeField] private CollectibleGenerationController    _CollectibleGenerationController = null;
    [SerializeField] private ScoreController                    _ScoreController = null;
    [SerializeField] private CameraController                   _CameraController = null;
    [SerializeField] private SectionController                  _SectionController = null;
    [SerializeField] private float                              _GameSpeed = 1f;

    private float       _DeltaTime = 0f;
    private GameState   _CurrentGameState = GameState.None;
    private float       _Countdown = 3f;

    public float                            DeltaTime => _DeltaTime;
    public ObstacleGenerationController     ObstacleGenerationController => _ObstacleGenerationController;
    public CollectibleGenerationController  CollectibleGenerationController => _CollectibleGenerationController;
    public GameState                        GameState => _CurrentGameState;
    public SectionType                      SectionType => _SectionController.SectionType;

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Debug.LogError("Vytvoril se dalsi GameController!!!");
        }

        _SectionController.OnSectionChanged += OnSectionChanged;

        _Player.OnPlayerHit += OnPlayerHit;
        _Player.OnCollectiblesChanged += CollectiblesChanged;
        _ScoreController.OnScoreDistanceChanged += ScoreDistanceChanged;
        _ScoreController.OnScoreHitChanged += ScoreHitChanged;
        _ScoreController.OnDataHitWarningChanged += DataHitWarningChanged;
        _ObstacleGenerationController.OnVirusSectionDataChanged += VirusSectionDataChanged;
        _ObstacleGenerationController.OnVirusHit += VirusHit;
        _CorridorGenerationController.OnCorridorBelowInPrerun += OnCorridorBelowInPrerun;
        _CurrentGameState = GameState.None;

        StartCoroutine(StartCountdown());
    }

    private void Update()
    {
        if (_CurrentGameState != GameState.Playing)
            return;

        _DeltaTime = Time.deltaTime * _GameSpeed;

        _SectionController.UpdateSC();
        _ObstacleGenerationController.UpdateCGC();
        _Player.UpdatePlayer();
        _CorridorGenerationController.UpdateCGC();
        _ScoreController.UpdateSC();
    }

    private void LateUpdate()
    {
        if (_CurrentGameState != GameState.Playing)
            return;

        _CameraController.LateUpdateCC();
    }

    private void OnPlayerHit(Obstacle pObstacle)
    {
        Debug.Log("GameController player hit");

        _CurrentGameState = GameState.End;

        SceneManager.LoadScene("MainLevel");
    }

    private void CollectiblesChanged(Dictionary<CollectibleType, int> pCollectibles)
    {
        OnCollectiblesChanged?.Invoke(pCollectibles);
    }

    private IEnumerator StartCountdown()
    {
        float tempCountdown = _Countdown;
        while (true)
        {
            _Countdown -= Time.deltaTime;

            if (_Countdown < 0f)
            {
                break;
            }

            yield return null;
        }

        _Countdown = tempCountdown;
        _CurrentGameState = GameState.Playing;
        _SectionController.SetSection(SectionType.Run);
    }

    private void SetGameState(GameState pState)
    {
        _CurrentGameState = pState;

        OnGameStateChanged?.Invoke(pState);
    }

    private void OnSectionChanged(SectionType pSection)
    {
        OnSectionTypeChanged?.Invoke(pSection);
    }

    private void ScoreDistanceChanged(int pScore)
    {
        OnScoreDistanceChanged?.Invoke(pScore);
    }

    private void ScoreHitChanged(int pScore)
    {
        OnScoreHitChanged?.Invoke(pScore);
    }

    private void DataHitWarningChanged(int pScore)
    {
        OnDataHitWarningChanged?.Invoke(pScore);
    }

    private void VirusSectionDataChanged(VirusSectionData pData)
    {
        OnVirusSectionDataChanged?.Invoke(pData);
    }

    private void VirusHit(VirusType pVirus)
    {
        OnVirusHit?.Invoke(pVirus);
    }

    private void OnCorridorBelowInPrerun()
    {
        _SectionController.SetSection(SectionType.Run);
    }

    public void LastCorridorReached()
    {
        if (SectionType == SectionType.PreShooter)
        {
            OnLastCorridorReached?.Invoke();
        }
    }
}
