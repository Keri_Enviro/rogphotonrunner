﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class CorridorGenerationController : MonoBehaviour
{
    public Action OnCorridorBelowInPrerun;

    [SerializeField] private Transform  _Parent = null;
    [SerializeField] private Corridor[] _CorridorPrefabs = null;
    [SerializeField] private Corridor[] _StartCorridorPrefabs = null;
    [SerializeField] private Corridor[] _LastCorridorPrefabs = null;

    private List<Corridor>  _Corridors = new List<Corridor>();
    private Corridor        _StartCorridor = null;
    private Corridor        _LastCorridor = null;

    private Dictionary<CorridorType, List<Corridor>>    _CorridorsDict = new Dictionary<CorridorType, List<Corridor>>();
    private Dictionary<int, List<Corridor>>             _CorridorsPool = new Dictionary<int, List<Corridor>>();

    private Vector3 _StartCorridorPosition = new Vector3(0, 0, -5f);

    private void Start()
    {
        for (int i = 0; i < _CorridorPrefabs.Length; i++)
        {
            Corridor cor = _CorridorPrefabs[i];
            cor.SetID(i);
            List<Corridor> list;
            if (_CorridorsDict.TryGetValue(cor.CorridorType, out list))
            {
                list.Add(cor);
            }
            else
            {
                list = new List<Corridor>();
                list.Add(cor);
                _CorridorsDict.Add(cor.CorridorType, list);
            }
        }

        GameObject goStart = Instantiate(_StartCorridorPrefabs[0].gameObject, new Vector3(0, 0, -20), Quaternion.identity);
        Corridor start = goStart.GetComponent<Corridor>();
        _StartCorridor = start;
        _StartCorridor.Init(false, 1);

        GameObject goLast = Instantiate(_LastCorridorPrefabs[0].gameObject, new Vector3(0, 0, -20), Quaternion.identity);
        Corridor last = goLast.GetComponent<Corridor>();
        _LastCorridor = last;
        _LastCorridor.Init(false, 1);

        CreateFirstCorridors();

        GameController.Get().OnSectionTypeChanged += OnSectionTypeChanged;
    }

    public void UpdateCGC()
    {
        if (GameController.Get().SectionType != SectionType.Shoot)
        {
            for (int i = 0; i < _Corridors.Count; i++)
            {
                Corridor cor = _Corridors[i];
                if (cor.IsActive)
                    cor.UpdateCorridor();
            }
        }

        if (GameController.Get().SectionType == SectionType.Run)
        {
            Corridor corridor = _Corridors[0];
            if (corridor.Connection.position.z < -8f)
            {
                _Corridors.RemoveAt(0);
                corridor.SetActive(false);
                CreateCorridor();
            }
        }
        else
        {
            _LastCorridor.UpdateCorridor();
        }

        if (GameController.Get().SectionType == SectionType.PreRun)
        {
            Corridor corridor = _Corridors[0];
            if (corridor.transform.position.z < 0f)
            {
                OnCorridorBelowInPrerun?.Invoke();
            }
        }
    }

    private void OnSectionTypeChanged(SectionType pSection)
    {
        switch (pSection)
        {
            case SectionType.Shoot:
                while (_Corridors.Count > 0)
                {
                    _Corridors[0].SetActive(false);
                    _Corridors.RemoveAt(0);
                }
                break;
            case SectionType.PreRun:
                _StartCorridorPosition = new Vector3(0,0,100);
                CreateFirstCorridors();

                _LastCorridor.LastMoveUp();
                break;
            case SectionType.Run:
                _LastCorridor.LastMoveBehind();
                break;
            case SectionType.PreShooter:
                _LastCorridor.Init(true, 1);
                Vector3 lastPosition = _Corridors[_Corridors.Count - 1].Connection.position;
                _LastCorridor.transform.position = lastPosition;
                break;
        }
    }

    private void CreateFirstCorridors()
    {
        for (int i = 0; i < 15; i++)
        {
            CreateCorridor();
        }
    }

    /// <summary>
    /// Vytvor corridor a vraz ho nakonec seznamu _Corridors
    /// </summary>
    private void CreateCorridor()
    {
        Corridor cor = null;

        if (_Corridors.Count == 0)
        {
            cor = _StartCorridor;
            cor.Init(true, cor.UniqueID);
            cor.transform.position = _StartCorridorPosition;
        }
        else
        {
            Corridor last = _Corridors[_Corridors.Count - 1];
            CorridorType[] corTypes = last.AllowedCorridorTypes;

            int index = Random.Range(0, corTypes.Length);
            List<Corridor> potencialCors = _CorridorsDict[corTypes[index]];

            index = Random.Range(0, potencialCors.Count);
            Corridor resultCor = potencialCors[index];

            int uniqueID = resultCor.UniqueID;
            if (_CorridorsPool.TryGetValue(uniqueID, out potencialCors))
            {
                cor = GetDisabledCorridor(potencialCors);
                if (cor != null)
                {
                    cor.SetID(uniqueID);
                    cor.transform.position = last.Connection.position;
                }
                else
                {
                    cor = InstantiateCorridor(resultCor, last.Connection.position);
                    potencialCors.Add(cor);
                }
            }
            else
            {
                potencialCors = new List<Corridor>();
                cor = InstantiateCorridor(resultCor, last.Connection.position);
                potencialCors.Add(cor);
                _CorridorsPool.Add(cor.UniqueID, potencialCors);
            }
        }

        cor.Init(true, cor.UniqueID);
        _Corridors.Add(cor);
    }

    private Corridor GetDisabledCorridor(List<Corridor> pList)
    {
        Corridor result = null;
        for (int i = 0; i < pList.Count; i++)
        {
            Corridor cor = pList[i];
            if (!cor.IsActive)
            {
                result = cor;
                break;
            }
        }

        return result;
    }

    private Corridor InstantiateCorridor(Corridor pPrefab, Vector3 pPosition)
    {
        GameObject go = Instantiate(pPrefab.gameObject, pPosition, Quaternion.identity, _Parent);
        Corridor cor = go.GetComponent<Corridor>();
        cor.SetID(pPrefab.UniqueID);

        return cor;
    }
}
