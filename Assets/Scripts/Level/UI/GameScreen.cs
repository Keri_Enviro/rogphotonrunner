﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : ScreenBase
{
    [SerializeField]
    private Text _ScoreDistance = null;

    [SerializeField]
    private Text _BulletCount = null;

    public void SetScoreText(int pScore)
    {
        _ScoreDistance.text = pScore.ToString();
    }

    public void SetBulletCount(int pScore)
    {
        _BulletCount.text = pScore.ToString();
    }
}
