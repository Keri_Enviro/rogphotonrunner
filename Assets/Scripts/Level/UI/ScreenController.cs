﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    [SerializeField]
    private GameScreen _GameScreen = null;

    private void Start()
    {
        GameController.Get().OnScoreDistanceChanged += OnScoreDistanceChanged;
        GameController.Get().OnCollectiblesChanged += OnCollectiblesChanged;
    }

    private void OnScoreDistanceChanged(int pScore)
    {
        _GameScreen.SetScoreText(pScore);
    }

    private void OnCollectiblesChanged(Dictionary<CollectibleType, int> pCollectibles)
    {
        int count;
        pCollectibles.TryGetValue(CollectibleType.Bullet, out count);
        _GameScreen.SetBulletCount(count);
    }
}
