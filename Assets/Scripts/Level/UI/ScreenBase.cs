﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScreenBase : MonoBehaviour
{
    [SerializeField]
    private Canvas _Canvas = null;

    public virtual void OpenScreen()
    {
        _Canvas.enabled = true;
    }

    public virtual void CloseScreen()
    {
        _Canvas.enabled = false;
    }
}
