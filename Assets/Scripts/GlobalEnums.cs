﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    None = 0,
    Playing = 1,
    Paused = 2,
    End = 3,
}

public enum SectionType
{
    None = 0,
    Run = 1,
    Shoot = 2,
    PreRun = 3,
    PreShooter = 4,
}

public enum VirusType
{
    None = 0,
    Virus = 1,
    Data = 2,
}

public enum FirewallCheckType
{
    None = 0,
    Testing = 1,
    Checked = 2,
}

public enum CameraState
{
    None = 0,
    FPV = 1, // first person view
    TPV = 2, // third person view
    Blending = 3,
}

public enum CollectibleType
{
    None = 0,
    Bullet = 1,
    Point = 2,
    Coin = 3,
}

public enum CorridorType
{
    None = 0,
    LastCorridor = 1,
    Ground = 2,
    RandomObstacle = 3,
    WithHole = 4,
    WithObstacle = 5,
    StartCorridor = 6,
}
